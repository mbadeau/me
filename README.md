# 👓 About Me

Inspiration (mostly lifted) [from](https://gitlab.com/kcomoli/tasks) [others](https://gitlab.com/dreedy/plate), I created this project to track things related to me. Including tasks, projects, and learning.

# This project

I'm going to try to make this into a Jekyll page. Hopefully I can generate some stats to show progress and auto update the weekly tasks.

### Pipeline status

[![pipeline status](https://gitlab.com/mbadeau/me/badges/master/pipeline.svg)](https://gitlab.com/mbadeau/me/-/commits/master)
